# Python/Django module for integration with SAP vie EDI (IDoc) interface

This module parses IDoc XML using Python schema modules generated for 
corresponding XSD files with generateDS.py (https://pypi.python.org/pypi/generateDS).

Example:

```
generateDS.py -o ts/edi/idoc_ds_modules/z_ts_hours.py ts/edi/assets/xsd/z_ts_hours.xsd
```