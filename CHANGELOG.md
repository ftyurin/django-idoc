# Change Log


## 0.1.8 - 2016-08-16

### Changed

- parse_idoc_value extended functionality with compatibility


## 0.1.7 - 2016-08-10

### Added

- parse_idoc_number and parse_idoc_value utils


## 0.1.6 - 2016-08-04

### Added

- logging for the idoc_content in utils.process function


## 0.1.5 - 2016-07-05

### Added

- parse_idoc_datetime is checking Django USE_TZ settings and convert naive datetime
  coming from SAP to current time zone.


## 0.1.4 - 2016-06-27

### Changed

- Empty 200 response on GET requests


## 0.1.3 - 2015-10-25

### Changed

- Tasks import


## 0.1.2 - 2015-10-20

### Added

- IDOC_EDI_DC40_SNDPOR, IDOC_EDI_DC40_SNDPRN, IDOC_EDI_DC40_RCVPOR, IDOC_EDI_DC40_RCVPRN
  settings
- IDocException (currently thrown only when idoc sending fails)

### Changed

- Send authentication header only when IDOC_OUTBOUND_USERNAME settings is set.


## 0.1.1 - 2015-09-21

### Added

- Basic HTTP authorization support (IDOC_AUTH_PASSWORD & IDOC_AUTH_PASSWORD settings)


## 0.1.0 - 2015-08-21

### Added

- Initial version
