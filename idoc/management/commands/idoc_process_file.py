from django.core.management.base import BaseCommand, CommandError

from ...utils import process


class Command(BaseCommand):
    help = 'Reads IDoc from file and processes it.'

    def add_arguments(self, parser):
        parser.add_argument('file_name', type=str)

    def handle(self, *args, **options):
        with open(options['file_name']) as f:
            process(f.read())
