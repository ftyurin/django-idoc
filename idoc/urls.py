from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns(
    '',

    url(r'^$', views.inbound_index, name='inbound_index'),
)
