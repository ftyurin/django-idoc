from celery.task import task

from . import utils


@task
def process(idoc_content):
    return utils._process(idoc_content)


@task
def send(idoc_content):
    return utils._send(idoc_content)
