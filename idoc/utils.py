import base64
import httplib
import logging
import random
import re
import sys

from datetime import datetime, date
from StringIO import StringIO
from xml import sax

from django.conf import settings
from django.utils.timezone import get_current_timezone


logger = logging.getLogger(__name__)


BACKGROUND_PROCESSING_ENABLED = getattr(settings, 'IDOC_BACKGROUND_PROCESSING_ENABLED', False)
OUTBOUND_HOST = getattr(settings, 'IDOC_OUTBOUND_HOST', None)
OUTBOUND_PATH = getattr(settings, 'IDOC_OUTBOUND_PATH', None)
OUTBOUND_USERNAME = getattr(settings, 'IDOC_OUTBOUND_USERNAME', None)
OUTBOUND_PASSWORD = getattr(settings, 'IDOC_OUTBOUND_PASSWORD', None)
EDI_DC40_SNDPOR = getattr(settings, 'IDOC_EDI_DC40_SNDPOR', None)
EDI_DC40_SNDPRN = getattr(settings, 'IDOC_EDI_DC40_SNDPRN', None)
EDI_DC40_RCVPOR = getattr(settings, 'IDOC_EDI_DC40_RCVPOR', None)
EDI_DC40_RCVPRN = getattr(settings, 'IDOC_EDI_DC40_RCVPRN', None)


IDOC_DS_MODULES = {}
IDOC_HANDLERS = {}


class IDocException(Exception):
    pass


def add_idoc_processor(ds_module, handler, root_tag):
    assert root_tag not in IDOC_DS_MODULES
    IDOC_DS_MODULES[root_tag] = ds_module
    idoc_class_name = ds_module.__all__[-1]
    assert idoc_class_name not in IDOC_HANDLERS
    IDOC_HANDLERS[idoc_class_name] = handler


def parse_idoc_value(obj, attr=None, default=''):
    value = getattr(obj, attr) if not attr is None else obj
    if isinstance(value, str): value = value.strip()
    return value or default


def parse_idoc_number(value):
    return value.lstrip('0')


def parse_idoc_date(date_str):
    if date_str == '00000000':
        return None
    return datetime.strptime(date_str, '%Y%m%d').date()


def parse_idoc_time(time_str):
    if time_str == '000000':
        return None
    return datetime.strptime(time_str, '%H%M%S').time()


def parse_idoc_datetime(date_str, time_str):
    if(time_str == None):
        time_str = '000000'

    if(date_str == None):
        date_str = '00000000'

    if date_str == '00000000' and time_str == '000000':
        return None

    dt = datetime.strptime(date_str + (time_str if time_str != '240000' else '000000'),
                             '%Y%m%d%H%M%S')

    if not settings.USE_TZ:
        return dt

    return dt.replace(tzinfo=get_current_timezone())


def parse(idoc_content):
    class IDocBasicHandler(sax.handler.ContentHandler):
        class CompleteException(Exception):
            def __init__(self, root_tag):
                super(IDocBasicHandler.CompleteException, self).__init__('OK')
                self.root_tag = root_tag

        def startElementNS(self, name, qname, attrs):
            raise IDocBasicHandler.CompleteException(name)

    # Removing not allowed char entities (SAP produces at least \x08)
    idoc_content = re.sub(r'\\x08', '', idoc_content)

    content_handler = IDocBasicHandler()
    parser = sax.make_parser()
    parser.setFeature(sax.handler.feature_namespaces, 1)
    parser.setContentHandler(content_handler)
    try:
        parser.parse(StringIO(idoc_content))
    except IDocBasicHandler.CompleteException, e:
        ds_module = IDOC_DS_MODULES.get(e.root_tag)
        if ds_module is None:
            return None
        return ds_module.parseString(idoc_content)
    except sax.SAXException, e:
        raise
    except Exception, e:
        raise


def handle(idoc):
    handler = IDOC_HANDLERS[idoc.__class__.__name__]
    try:
        iter(idoc.IDOC)
    except TypeError:
        handler(idoc.IDOC)
    else:
        for idoc_item in idoc.IDOC:
            handler(idoc_item)


def _process(idoc_content):
    idoc = parse(idoc_content)
    if idoc is not None:
        handle(idoc)


def _send(idoc_content):
    headers = {
        'Content-Type': 'text/xml',
    }
    if OUTBOUND_USERNAME is not None:
        headers['Authorization'] = 'Basic ' + base64.b64encode(
            OUTBOUND_USERNAME + ':' + OUTBOUND_PASSWORD)

    start_dt = datetime.now()

    conn = httplib.HTTPConnection(OUTBOUND_HOST)
    conn.request('POST', OUTBOUND_PATH, idoc_content, headers)
    resp = conn.getresponse()
    resp_status = resp.status
    resp_content = resp.read()

    end_dt = datetime.now()

    if resp_status >= 400:
        raise IDocException(resp_content)

    return resp_content


def process(idoc_content):
    logger.info(idoc_content)
    if BACKGROUND_PROCESSING_ENABLED:
        from .tasks import process as process_task
        process_task.delay(idoc_content)
        return
    _process(idoc_content)


def send(idoc_content):
    if BACKGROUND_PROCESSING_ENABLED:
        from .tasks import send as send_task
        send_task.delay(idoc_content)
        return
    _send(idoc_content)


def get_idoc_content(idoc):
    output = StringIO()
    print >>output, '<?xml version="1.0" encoding="UTF-8"?>'
    idoc.export(output, 0, pretty_print=False)
    content = output.getvalue()
    output.close()
    return content


def create_edi_dc40(ds, idoc_type=None,
    SEGMENT=None,
    TABNAM=None,
    MANDT=None,
    DOCNUM=None,
    DOCREL=None,
    STATUS=None,
    DIRECT=None,
    OUTMOD=None,
    EXPRSS=None,
    TEST=None,
    IDOCTYP=None,
    CIMTYP=None,
    MESTYP=None,
    MESCOD=None,
    MESFCT=None,
    STD=None,
    STDVRS=None,
    STDMES=None,
    SNDPOR=None,
    SNDPRT=None,
    SNDPFC=None,
    SNDPRN=None,
    SNDSAD=None,
    SNDLAD=None,
    RCVPOR=None,
    RCVPRT=None,
    RCVPFC=None,
    RCVPRN=None,
    RCVSAD=None,
    RCVLAD=None,
    CREDAT=None,
    CRETIM=None,
    REFINT=None,
    REFGRP=None,
    REFMES=None,
    ARCKEY=None,
    SERIAL=None):
    current_dt = datetime.now()
    edi_dc40 = ds.EDI_DC40Type(
        SEGMENT=1 if SEGMENT is None else SEGMENT,
        TABNAM='EDI_DC40' if TABNAM is None else TABNAM,
        MANDT='800' if MANDT is None else MANDT,
        DOCNUM=str(int(random.uniform(0, 9999999999999999))) if DOCNUM is None else DOCNUM,
        DOCREL='700' if DOCREL is None else DOCREL,
        STATUS='30' if STATUS is None else STATUS,
        DIRECT=1 if DIRECT is None else DIRECT,
        OUTMOD='1' if OUTMOD is None else OUTMOD,
        EXPRSS=None if EXPRSS is None else EXPRSS,
        TEST=None if TEST is None else TEST,
        IDOCTYP=idoc_type if IDOCTYP is None else IDOCTYP,
        CIMTYP=None if CIMTYP is None else CIMTYP,
        MESTYP=idoc_type if MESTYP is None else MESTYP, # Same as idoc type if not set
        MESCOD=None if MESCOD is None else MESCOD,
        MESFCT=None if MESFCT is None else MESFCT,
        STD=None if STD is None else STD,
        STDVRS=None if STDVRS is None else STDVRS,
        STDMES=None if STDMES is None else STDMES,
        SNDPOR=EDI_DC40_SNDPOR if SNDPOR is None else SNDPOR,
        SNDPRT='LS' if SNDPRT is None else SNDPRT,
        SNDPFC=None if SNDPFC is None else SNDPFC,
        SNDPRN=EDI_DC40_SNDPRN if SNDPRN is None else SNDPRN,
        SNDSAD=None if SNDSAD is None else SNDSAD,
        SNDLAD=None if SNDLAD is None else SNDLAD,
        RCVPOR=EDI_DC40_RCVPOR if RCVPOR is None else RCVPOR,
        RCVPRT='LS' if RCVPRT is None else RCVPRT,
        RCVPFC=None if RCVPFC is None else RCVPFC,
        RCVPRN=EDI_DC40_RCVPRN if RCVPRN is None else RCVPRN,
        RCVSAD=None if RCVSAD is None else RCVSAD,
        RCVLAD=None if RCVLAD is None else RCVLAD,
        CREDAT=current_dt.strftime('%Y%m%d') if CREDAT is None else CREDAT,
        CRETIM=current_dt.strftime('%H%M%S') if CRETIM is None else CRETIM,
        REFINT=None if REFINT is None else REFINT,
        REFGRP=None if REFGRP is None else REFGRP,
        REFMES=None if REFMES is None else REFMES,
        ARCKEY=None if ARCKEY is None else ARCKEY,
        SERIAL=None if SERIAL is None else SERIAL)
    return edi_dc40
