from functools import wraps

from django.conf import settings
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotAllowed
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from . import utils


AUTH_USERNAME = getattr(settings, 'IDOC_AUTH_USERNAME', None)
AUTH_PASSWORD = getattr(settings, 'IDOC_AUTH_PASSWORD', None)


def auth(func):
    @wraps(func)
    def _decorator(request, *args, **kwargs):
        if AUTH_USERNAME is not None:
            if request.META.has_key('HTTP_AUTHORIZATION'):
                authmeth, auth = request.META['HTTP_AUTHORIZATION'].split(' ', 1)
                if authmeth.lower() == 'basic':
                    auth = auth.strip().decode('base64')
                    username, password = auth.split(':', 1)
                    if username != AUTH_USERNAME or password != AUTH_PASSWORD:
                        return HttpResponseForbidden('Forbidden')
            else:
                r = HttpResponse(status=401)
                r['WWW-Authenticate'] = 'Basic'
                return r
        return func(request, *args, **kwargs)
    return _decorator


@auth
@csrf_exempt
def inbound_index(request):
    if request.method == 'POST':
        utils.process(request.body)
        return HttpResponse('')
    elif request.method == 'GET':
        return HttpResponse('')
    return HttpResponseNotAllowed(['GET', 'POST'])
