# -*- coding: utf-8 -*-
import os
from distutils.core import setup


__author__ = 'Fedor Tyurin'
__version__ = '0.1.6'


os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name='idoc',
    version=__version__,
    packages=['idoc', 'idoc.management', 'idoc.management.commands', 'idoc.migrations'],
    url='http://172.25.34.141/acando/idoc',
    license='MIT',
    author=__author__,
    author_email='fedor.tyurin@acando.com',
    description='Python/Django module for integration with SAP vie EDI (IDoc) interface',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'Django>=1.8',
    ],
)
